/**
 * Created by enixjin
 */
import * as winston from "winston";
import {Server} from "http";
import * as multer from "multer";
import * as ffmpeg from "ffmpeg-ts";

require('source-map-support').install();
let config = global.config;

let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

let app = express();
let http = require('http');

const fs = require('fs');

const resource_path = "/resources";

// uncomment after placing your favicon in /public
app.use(favicon(path.join('static', 'favicon.ico')));
//app.use(logger('dev'));
app.set('view engine', 'ejs');
app.use(logger('combined', {
    skip: function (req, res) {
        return res.statusCode < 400
    }
}));
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(resource_path, express.static(global.config.uploadPath));
app.use(express.static('static'));


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    if (req.method === "OPTIONS") {
        res.end();
    } else {
        next();
    }
});
let storage = multer.diskStorage({
    destination: global.config.uploadPath,
    filename: function (req, file, cb) {
        let filename = file.originalname.split('.')[0] + '.' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];
        filename = filename.replace(" ", "");
        cb(null, filename);
    }
});
let upload = multer({storage});
app.post('/convert', upload.single('videoFile'), async (req, res) => {
    let file = req.file;
    let target = file.filename.substring(0, file.filename.lastIndexOf('.'));
    await ffmpeg.convertVideo({
        source: file.path,
        format: "gif",
        target: file.destination + "/" + target,
        frameRate: "5",
        bitRate: "64k",
        scale: "160:-1"
    });
    res.end(`${resource_path}\/${target}.gif`);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    //var err = new Error('Not Found');
    //err.status = 404;
    let err = {error: "ERR_404"};
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    res.status(500);
    winston.error(err);
    res.jsonp({error: "ERR_500"});
});

function start(): Server {
    /**
     * Get port from environment and store in Express.
     */

    let port = normalizePort(config.servicePort || '3000');
    app.set('port', port);

    /**
     * Create HTTP server.
     */

    let server = http.createServer(app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);

    process.on('unhandledRejection', (reason, p) => {
        winston.error(`Unhandled Rejection at: Promise ${p} reason: ${reason}`);
    });

    /**
     * Normalize a port into a number, string, or false.
     */

    function normalizePort(val) {
        let port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        let bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                winston.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                winston.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        winston.info("ffmpeg-ts demo Server started at " + config.servicePort);
    }

    return server;

}

module.exports = {
    start: start
};

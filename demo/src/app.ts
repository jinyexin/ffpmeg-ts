/**
 * Created by enixjin.
 */

let config = require('../config.js');
import * as winston from 'winston';
import * as fs from "fs-extra";

global.config = config;

let transports = [];
if (config.logFile) {
    fs.ensureFileSync(config.logFile);
    const files = new winston.transports.File({
        filename: config.logFile,
        json: false,
        maxsize: 400000,
        maxFiles: 10,
        level: config.logLevelFile ? config.logLevelFile : config.logLevel,
        timestamp: function () {
            return new Date().toLocaleString();
        }
    });
    transports.push(files);

}

const console = new winston.transports.Console({
    json: false,
    prettyPrint: true,
    colorize: true,
    silent: false,
    level: config.logLevel,
    timestamp: function () {
        return new Date().toLocaleString();
    }
});
transports.push(console);

winston.configure({
    transports
});

winston.info("======================================================");
winston.info("==            start ffmpeg-ts demo server           ==");
winston.info("======================================================");
declare const module: any;
let server = require("./server").start();
//hot reload
if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => server.close());
}

/**
 * Created by enixjin.
 */
var config = {};

config.servicePort = 8000;

config.logLevel = 'debug';

config.logFile = "/sandbox/temp/log.log";
config.logLevelFile = "debug";

config.uploadPath = "/sandbox/temp/upload";

module.exports = config;

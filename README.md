# ffmpeg-ts

A typescript library for most common used ffmpeg commands.

(Suggest use typescript, but you can call this lib with js too) 

Description:

  Objective of this libaray is to provide a simple but typed ffmpeg commands wrapper. 
  
  Just most common used, no intends to become a full abstraction of ffmpeg.
  
  A demo could be found [here](https://vtog.enixjin.net/)

Notice: You should install ffmpeg in your server first by you own. 

(e.g. for Ubuntu: sudo apt install ffmpeg)
    
## Current support commands 

see: [Test Case](https://gitlab.com/jinyexin/ffpmeg-ts/tree/master/src/test) for more use case

### extract audio from video

    import * as ffmpegts from "ffmpeg-ts";
    
    async somtFunction(){
      ...
      let audioFilePath = await ffmpegts.extract(
      {
         source: PathToVideoMP4,
         cut: {
             start: 0,
             length: 2
         }
      });
      ...
    }

### convert audio/video format

    import * as ffmpeg from "ffmpeg-ts";
        
    async somtFunction(){
      ...
      //video
      let aviFilePath = await ffmpeg.convertVideo({source: PathToVideoMP4, format: "avi"});
      let mpgFilePath = await ffmpeg.convertVideo({source: PathToVideoMP4, format: "mpg"});
      let wmvFilePath = await ffmpeg.convertVideo({source: PathToVideoMP4, format: "wmv"});
      let mkvFilePath = await ffmpeg.convertVideo({source: PathToVideoMP4, format: "mkv"});
      let gifFilePath = await ffmpeg.convertVideo({
                                         source: PathToVideoMP4, 
                                         format: "gif",
                                         cut: {  //only cut first 2 second as gif
                                             start: 0,
                                             length: 2
                                         }
                                     });
      
      //audio
      let oggFilePath = await ffmpeg.convertAudio({source: PathToAudioMP3, format: "ogg"});
      let aacFilePath = await ffmpeg.convertAudio({source: PathToAudioMP3, format: "aac"});
      ...
    }
    
### utility function
    
    let format: string = await ffmpeg.getAudioFormat(source);
    let duration: number = await ffmepg.getDurationInSeconds(source);
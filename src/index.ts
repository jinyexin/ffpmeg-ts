/**
 * Created by enixjin on 12/27/2018
 */
import {format} from "winston";
import * as winston from "winston";


let consoleFormat = format.combine(
    format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.prettyPrint(),
    format.colorize(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);
let transports = [];
const console = new winston.transports.Console({
    format: consoleFormat,
    level: "debug",
});
transports.push(console);
winston.configure({
    transports
});

export * from "./commands/extract";
export * from "./commands/convert";
export * from "./utils";
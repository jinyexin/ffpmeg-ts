/**
 * Created by enixjin on 1/10/2019
 */
import {convertVideoOption} from "./commands/convert";

export function getConvertConfig240p(source, target, format): convertVideoOption {
    return {
        frameRate: "24",
        scale: "320x240",
        source,
        target,
        format
    }
}

export function getConvertConfig360p(source, target, format): convertVideoOption {
    return {
        frameRate: "24",
        scale: "640x360",
        source,
        target,
        format
    }
}

export function getConvertConfig480p(source, target, format): convertVideoOption {
    return {
        frameRate: "24",
        scale: "720x480",
        source,
        target,
        format
    }
}

export function getConvertConfig720p(source, target, format): convertVideoOption {
    return {
        frameRate: "24",
        scale: "1280x720",
        source,
        target,
        format
    }
}

export function getConvertConfig1080p(source, target, format): convertVideoOption {
    return {
        frameRate: "24",
        scale: "1920x1080",
        source,
        target,
        format
    }
}
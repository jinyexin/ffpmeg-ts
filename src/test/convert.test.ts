/**
 * Created by enixjin on 12/27/2018
 */

import * as ffmpeg from "../";
import * as fs from "fs-extra";
import {getConvertConfig1080p, getConvertConfig240p} from "../";

describe("convert test", function () {
    this.timeout(15000);
    before(function () {
    });
    it("mp4 to avi", (done) => {
        ffmpeg
            .convertVideo({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                format: "avi",
                target: __dirname + "/files/output/SampleVideo_1mb"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to avi");
                }
            }, error => done(error));
    });
    it("mp4 to avi 240p", (done) => {
        ffmpeg
            .convertVideo(
                getConvertConfig240p(
                    __dirname + "/files/SampleVideo_1mb.mp4",
                    __dirname + "/files/output/SampleVideo_1mb_240p",
                    "avi"
                ))
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to avi");
                }
            }, error => done(error));
    });
    it("mp4 to avi 1080p", (done) => {
        ffmpeg
            .convertVideo(
                getConvertConfig1080p(
                    __dirname + "/files/SampleVideo_1mb.mp4",
                    __dirname + "/files/output/SampleVideo_1mb_1080p",
                    "avi"
                ))
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to avi");
                }
            }, error => done(error));
    });
    it("mp4 to mpg bitRate 64k", (done) => {
        ffmpeg
            .convertVideo({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                format: "mpg",
                target: __dirname + "/files/output/SampleVideo_1mb_bitrate",
                bitRate: "64k"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to avi");
                }
            }, error => done(error));
    });
    it("mp4 to mpg", (done) => {
        ffmpeg
            .convertVideo({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                format: "mpg",
                target: __dirname + "/files/output/SampleVideo_1mb"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to mpg");
                }
            }, error => done(error));
    });
    it("mp4 to mkv", (done) => {
        ffmpeg
            .convertVideo({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                format: "mkv",
                target: __dirname + "/files/output/SampleVideo_1mb"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to mkv");
                }
            }, error => done(error));
    });
    it("mp4 to mkv frameRate 24", (done) => {
        ffmpeg
            .convertVideo({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                format: "mkv",
                target: __dirname + "/files/output/SampleVideo_1mb_frameRate",
                frameRate: "24"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to mkv");
                }
            }, error => done(error));
    });
    it("mp4 to gif", (done) => {
        ffmpeg
            .convertVideo({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                format: "gif",
                target: __dirname + "/files/output/SampleVideo_1mb"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to gif");
                }
            }, error => done(error));
    });
    it("mp3 to ogg", (done) => {
        ffmpeg
            .convertAudio({
                source: __dirname + "/files/SampleAudio_0.7mb.mp3",
                format: "ogg",
                target: __dirname + "/files/output/SampleAudio_0.7mb"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp4 to ogg");
                }
            }, error => done(error));
    });
    it("mp3 to acc", (done) => {
        ffmpeg
            .convertAudio({
                source: __dirname + "/files/SampleAudio_0.7mb.mp3",
                format: "aac",
                target: __dirname + "/files/output/SampleAudio_0.7mb"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to convert mp3 to acc");
                }
            }, error => done(error));
    });
    after(function () {
        fs.emptyDirSync(__dirname + "/files/output");
    });
});
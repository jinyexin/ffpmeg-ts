/**
 * Created by enixjin on 12/27/2018
 */

import * as ffmpeg from "../";
import * as fs from "fs-extra";

describe("extract test", function () {
    this.timeout(15000);
    before(function () {
    });
    it("from mp4", (done) => {
        ffmpeg
            .extract({
                source: __dirname + "/files/SampleVideo_1mb.mp4",
                target: __dirname + "/files/output/SampleVideo_1mb",
                cut: {
                    start: 0,
                    length: 2
                }
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to extract voice file");
                }
            }, error => done(error));
    });
    it("from avi(pcm audio)", (done) => {
        ffmpeg
            .extract({
                source: __dirname + "/files/videoWithPcmAudio.avi",
                target: __dirname + "/files/output/videoWithPcmAudio"
            })
            .then(outFile => {
                if (fs.existsSync(outFile)) {
                    done();
                } else {
                    done("fail to extract voice file");
                }
            }, error => done(error));
    });
    after(function () {
        fs.emptyDirSync(__dirname + "/files/output");
    });
});
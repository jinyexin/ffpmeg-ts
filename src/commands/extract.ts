/**
 * Created by enixjin on 12/27/2018
 */
import * as winston from "winston";
import * as util from "util";
import {options} from "./options";

const exec = util.promisify(require('child_process').exec);

interface extractOption extends options {
}

/**
 * This method will extract audio from video with original format
 * @returns outputFile full path of output audio file
 * @param option
 */
export async function extract(option: extractOption): Promise<string> {
    let {source, target} = option;
    if (!target) {
        target = source.substr(0, source.lastIndexOf(".")).replace(" ", "");
    }
    let extraFlag = "";
    if (option.cut) {
        extraFlag += ` -ss ${option.cut.start} -t ${option.cut.length} `;
    }
    let format: string = await getAudioFormat(source);
    winston.debug(`audio format is:${format}`);
    if (["acc", "ogg", "mp3"].indexOf(format) > -1) {
        extraFlag += " -acodec copy "
    } else {
        winston.debug(`format not support, downgrade to mp3!`);
        format = "mp3";
    }
    const {stdout, stderr} = await exec(`ffmpeg -y -i "${source}" -vn ${extraFlag} ${target}.${format}`);
    if (stderr) {
        winston.error(stderr);
        throw new Error("fail to parse video file");
    }
    winston.debug(`extracted audio file:  ${target}.${format}`);
    return `${target}.${format}`;
}

export async function getAudioFormat(inputVideo: string): Promise<string> {
    const {stdout, stderr} = await exec(`ffprobe "${inputVideo}" 2>&1 | grep Audio`);
    if (stderr) {
        winston.error(stderr);
        throw new Error(stderr);
    }
    if (!stdout || stdout.indexOf("Audio:") === -1) {
        winston.error(`fail to find audio format!`);
        throw new Error(`fail to find audio format!`);
    }
    return stdout.substr(stdout.indexOf("Audio:") + 7, 3);
}

export async function getDurationInSeconds(inputVideo: string): Promise<number> {
    const {stdout, stderr} = await exec(`ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "${inputVideo}"`);
    if (stderr) {
        winston.error(stderr);
        throw new Error(stderr);
    }
    return parseFloat(stdout);
}

/**
 * Created by enixjin on 12/28/2018
 */

export type options = {
    source: string,
    format?: string,
    target?: string,
    cut?: {
        start: number,
        length: number
    }
}
/**
 * Created by enixjin on 12/27/2018
 */
import * as winston from "winston";
import * as util from "util";
import {options} from "./options";

const exec = util.promisify(require('child_process').exec);

export interface convertVideoOption extends options {
    format: "avi" | "wmv" | "mpg" | "mkv" | "gif",
    scale?: "320x240" | "160x90" | "640x360" | "720x480" | "800x600" | "1280x720" | "1920x1080",
    frameRate?: "5" | "10" | "15" | "24" | "30";
    bitRate?: "64k" | "128k" | "256k"
}

interface convertAudioOption extends options {
    format: "mp3" | "ogg" | "aac"
}

/**
 * convert video format
 * @param option
 */
export async function convertVideo(option: convertVideoOption) {
    let {source, target, format} = option;
    if (!target) {
        target = source.substr(0, source.lastIndexOf("/")).replace(" ", "");
    }
    let extraFlag = "";
    if (option.scale) {
        extraFlag += ` -s ${option.scale} `;
    }
    if (option.cut) {
        extraFlag += ` -ss ${option.cut.start} -t ${option.cut.length} `;
    }
    if (option.frameRate) {
        extraFlag += ` -r ${option.frameRate} `;
    }
    if (option.bitRate) {
        extraFlag += ` -b:v ${option.bitRate} `;
    }
    const outFilename = target + "." + format;
    winston.debug(`ffmpeg -i "${source}" ${extraFlag} ${outFilename}`);
    const {stdout, stderr} = await exec(`ffmpeg -y -i "${source}" ${extraFlag} ${outFilename}`);
    if (stderr) {
        winston.error(stderr);
        throw new Error("fail to parse video file");
    }
    winston.debug(`converted video file:${outFilename}`);
    return outFilename;
}

/**
 * convert audio
 * @param option
 */
export async function convertAudio(option: convertAudioOption) {
    let {source, target, format} = option;
    if (!target) {
        target = source.substr(0, source.lastIndexOf("/")).replace(" ", "");
    }
    let extraFlag = "";
    if (option.cut) {
        extraFlag += ` -ss ${option.cut.start} -t ${option.cut.length} `;
    }
    const outFilename = target + "." + format;
    winston.debug(`ffmpeg -i "${source}" ${extraFlag} ${outFilename}`);
    const {stdout, stderr} = await exec(`ffmpeg -y -i "${source}" ${extraFlag} ${outFilename}`);
    if (stderr) {
        winston.error(stderr);
        throw new Error("fail to parse audio file");
    }
    winston.debug(`converted audio file:${outFilename}`);
    return outFilename;
}
